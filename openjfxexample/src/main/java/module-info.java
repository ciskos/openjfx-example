module io.ciskos.openjfxexample {
	
    requires javafx.controls;
    requires javafx.fxml;
    requires javafx.base;
    requires java.prefs;
    requires lombok;
    requires jakarta.xml.bind;

    opens io.ciskos.openjfxexample to javafx.fxml;
    opens io.ciskos.openjfxexample.view to javafx.fxml;
    opens io.ciskos.openjfxexample.model to jakarta.xml.bind;
    
    exports io.ciskos.openjfxexample;
    exports io.ciskos.openjfxexample.view;
    exports io.ciskos.openjfxexample.util;
    
}
