package io.ciskos.openjfxexample.view;

import io.ciskos.openjfxexample.MainApp;
import io.ciskos.openjfxexample.model.Person;
import io.ciskos.openjfxexample.util.DateUtil;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;

public class PersonOverviewController {

	@FXML
	private TableView<Person> personTable;
	@FXML
	private TableColumn<Person, String> firstNameColumn;
	@FXML
	private TableColumn<Person, String> lastNameColumn;
	@FXML
	private Label firstNameLabel;
	@FXML
	private Label lastNameLabel;
	@FXML
	private Label streetLabel;
	@FXML
	private Label postalCodeLabel;
	@FXML
	private Label cityLabel;
	@FXML
	private Label birthdayLabel;
	
	private MainApp mainApp;
	
	public PersonOverviewController() {
	}
	
	/**
	 * Инициализация класса-контроллера. Этот метод вызывается автоматически
	 * после того, как fxml-файл будет загружен.
	 * */
	@FXML
	private void initialize() {
		// Инициализация таблицы адресатов с двумя столбцами.
		firstNameColumn.setCellValueFactory(cellData -> cellData.getValue().firstNameProperty());
		lastNameColumn.setCellValueFactory(cellData -> cellData.getValue().lastNameProperty());
		
		// Очистка дополнительной информации об адресате.
		showPersonDetails(null);

		// Слушаем изменения выбора, и при изменении отображаем
		// дополнительную информацию об адресате.
		personTable.getSelectionModel().selectedItemProperty().addListener(
					(observable, oldValue, newValue) -> showPersonDetails(newValue));
	}
	
	/**
	 * Вызывается главным приложением, котрое на себя ссылку.
	 * 
	 * @param mainApp
	 * */
	// этот метод должен быть заменён Hibernate-ом
	public void setMainApp(MainApp mainApp) {
		this.mainApp = mainApp;
		
		// Добавление в таблицу данных из наблюдаемого списка
		personTable.setItems(mainApp.getPersonData());
	}
	
	/**
	 * Заполняет все текстовые поля, отображая подробности об адресает.
	 * Если указанный адресат равен null, то все текстовые поля очищаются.
	 * 
	 * 
	 * */
	private void showPersonDetails(Person person) {
		if (person != null) {
			// Заполняем метки информацией из объекта person.
			firstNameLabel.setText(person.getFirstName());
			lastNameLabel.setText(person.getLastName());
			streetLabel.setText(person.getStreet());
			postalCodeLabel.setText(person.getPostalCode() + "");
			cityLabel.setText(person.getCity());
			birthdayLabel.setText(DateUtil.format(person.getBirthday()));
		} else {
			// Если Person = null, то убрать весь текст
			firstNameLabel.setText("");
			lastNameLabel.setText("");
			streetLabel.setText("");
			postalCodeLabel.setText("");
			cityLabel.setText("");
			birthdayLabel.setText("");
		}
	}
	
	/**
	 * Вызывается, когда пользователь кликает по кнопке удаления.
	 * */
	@FXML
	private void handleDeletePerson() {
		int selectedIndex = personTable.getSelectionModel().getSelectedIndex();
		
		if (selectedIndex >= 0) {
			personTable.getItems().remove(selectedIndex);
		} else {
			// Ничего не выбрано.
			Alert alert = new Alert(AlertType.WARNING);
			
			alert.initOwner(mainApp.getPrimaryStage());
			alert.setTitle("No Selection");
			alert.setHeaderText("No Person Selected");
			alert.setContentText("Please select a person in the table.");
			
			alert.showAndWait();
		}
	}

	/**
	 * Вызывается, когда пользователь кликает по кнопке New...
	 * Открывает диалоговое окно с дополнительной информацией нового адресата.
	 * */
	@FXML
	private void handleNewPerson() {
		Person tempPerson = new Person();
		boolean okClicked = mainApp.showPersonEditDialog(tempPerson);
		
		if (okClicked) {
			mainApp.getPersonData().add(tempPerson);
		}
	}
	
	/**
	 * Вызывается, когда пользователь кликает по кнопке Edit...
	 * Открывает диалоговое окно для изменения выбранного адресата.
	 * */
	@FXML
	private void handleEditPerson() {
		Person selectedPerson = personTable.getSelectionModel().getSelectedItem();
		
		if (selectedPerson != null) {
			boolean okClicked = mainApp.showPersonEditDialog(selectedPerson);
			if (okClicked) {
				showPersonDetails(selectedPerson);
			}
		} else {
			// Ничего не выбрано.
			Alert alert = new Alert(AlertType.WARNING);
			alert.initOwner(mainApp.getPrimaryStage());
			alert.setTitle("No Selection");
			alert.setHeaderText("No Person Selected");
			alert.setContentText("Please select a person in the table.");
			
			alert.showAndWait();
		}
	}
	
}
