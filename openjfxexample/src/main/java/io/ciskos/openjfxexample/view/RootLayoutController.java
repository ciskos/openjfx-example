package io.ciskos.openjfxexample.view;

import java.io.File;
import java.io.IOException;

import io.ciskos.openjfxexample.MainApp;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.stage.FileChooser;
import lombok.Setter;

/**
 * Контроллер для корневого макета. Корневой макет предоставляет базовый
 * макет приложения, содержащий строку меню и место, где будут размещены
 * остальные элементы JavaFX.
 * 
 * @author kot
 * */
public class RootLayoutController {

	// Ссылка на главное приложение
	/**
	 * Вызывается главным приложением, чтобы оставить ссылку на самого себя.
	 * 
	 * @param mainApp
	 * */
	@Setter
	private MainApp mainApp;
	
	@FXML
	private void handleNew() {
		mainApp.getPersonData().clear();
		mainApp.setPersonalFilePath(null);
	}
	
	/**
	 * Открывает FileChooser, что пользователь имел возможность
	 * выбрать адресную книгу для загрузки.
	 * */
	@FXML
	private void handleOpen() {
		FileChooser fileChooser = new FileChooser();
		
		// Задаём фильтр расширений
		FileChooser.ExtensionFilter extFilter =
				new FileChooser.ExtensionFilter("XML files (*.xml)", "*.xml");
		
		// Показываем диалог загрузки файла
		File file = fileChooser.showOpenDialog(mainApp.getPrimaryStage());
		
		if (file != null) {
			mainApp.loadPersonDataFromFile(file);
		}
	}
	
	/**
	 * Сохраняет файл в файл адресатов, который в настоящее время открыт.
	 * Если файл не открыт, то отображается диалог "Save As".
	 * */
	@FXML
	private void handleSave() {
		File personFile = mainApp.getPersonFilePath();
		
		if (personFile != null) {
			mainApp.savePersonDataToFile(personFile);
		} else {
			handleSaveAs();
		}
	}
	
	/**
	 * Открывает FileChooser, чтобы пользователь имель возможность
	 * выбрать файл, куда будут сохранены данные.
	 * */
	@FXML
	private void handleSaveAs() {
		FileChooser fileChooser = new FileChooser();
		
		// Задаём фильтр расширений
		FileChooser.ExtensionFilter extFilter =
				new FileChooser.ExtensionFilter("XML files (*.xml)", "*.xml");
		
		fileChooser.getExtensionFilters().add(extFilter);
		
		// Показываем диалог сохранения файла
		File file = fileChooser.showSaveDialog(mainApp.getPrimaryStage());
		
		if (file != null) {
			// Убедиться в корректном расширении
			if (!file.getPath().endsWith(".xml")) {
				file = new File(file.getPath() + ".xml");
			}
			
			mainApp.savePersonDataToFile(file);
		}
	}
	
	/**
	 * Открывает диалоговое окно about.
	 * */
	@FXML
	private void handleAbout() {
		Alert alert = new Alert(AlertType.INFORMATION);
		
		alert.setTitle("AddressApp");
		alert.setHeaderText("About");
		alert.setContentText("Author: Kot\nWebsite: https://gitlab.io/ciskos");
		
		alert.showAndWait();
	}

	/**
	 * Закрывает приложение.
	 * */
	@FXML
	private void handleExit() {
		System.exit(0);
	}
	
	/**
	 * Открывает статистику дней рождений.
	 * @throws IOException 
	 * */
	@FXML
	private void handleShowBirthdayStatistics() throws IOException {
		mainApp.showBirthdayStatistics();
	}
	
}
