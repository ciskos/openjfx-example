package io.ciskos.openjfxexample.util;

import java.time.LocalDate;

import jakarta.xml.bind.annotation.adapters.XmlAdapter;

/**
 * Адаптер(для JAXB) для преобразования между типом LocalDate и строковым
 * представлением даты в стандарте ISO-8601, например как '2012-12-03'.
 * 
 * @author kot
 * */
public class LocalDateAdapter extends XmlAdapter<String, LocalDate> {

	@Override
	public LocalDate unmarshal(String v) throws Exception {
		return LocalDate.parse(v);
	}

	@Override
	public String marshal(LocalDate v) throws Exception {
		return v.toString();
	}

}
