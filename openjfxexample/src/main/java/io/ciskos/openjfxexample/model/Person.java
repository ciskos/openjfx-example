package io.ciskos.openjfxexample.model;

import java.time.LocalDate;

import io.ciskos.openjfxexample.util.LocalDateAdapter;
import jakarta.xml.bind.annotation.XmlType;
import jakarta.xml.bind.annotation.adapters.XmlJavaTypeAdapter;
import javafx.beans.property.IntegerProperty;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

@XmlType(propOrder = {"firstName", "lastName", "street", "postalCode", "city", "birthday"})
public class Person {

	private final StringProperty firstName;
	private final StringProperty lastName;
	private final StringProperty street;
	private final IntegerProperty postalCode;
	private final StringProperty city;
	private final ObjectProperty<LocalDate> birthday;

	public Person() {
		this(null, null);
	}
	
	public Person(String firstName, String lastName) {
		this.firstName = new SimpleStringProperty(firstName);
		this.lastName = new SimpleStringProperty(lastName);
		
		// Фиктивные начальные данные для удобства тестирования
		this.street = new SimpleStringProperty("some street");
		this.postalCode = new SimpleIntegerProperty(1234);
		this.city = new SimpleStringProperty("some city");
		this.birthday = new SimpleObjectProperty<>(LocalDate.of(1970, 01, 01));
	}

	/* SETTERS */
	public void setFirstName(String firstName) {
		this.firstName.set(firstName);
	}
	
	public void setLastName(String lastName) {
		this.lastName.set(lastName);
	}
	
	public void setStreet(String street) {
		this.street.set(street);
	}
	
	public void setPostalCode(int postalCode) {
		this.postalCode.set(postalCode);
	}
	
	public void setCity(String city) {
		this.city.set(city);
	}
	
	public void setBirthday(LocalDate birthday) {
		this.birthday.set(birthday);
	}

	/* GET PROPERTIES */
	public StringProperty firstNameProperty() {
		return firstName;
	}

	public StringProperty lastNameProperty() {
		return lastName;
	}

	public StringProperty streetProperty() {
		return street;
	}

	public IntegerProperty postalCodeProperty() {
		return postalCode;
	}

	public StringProperty cityProperty() {
		return city;
	}

	public ObjectProperty<LocalDate> birthdayProperty() {
		return birthday;
	}
	
	/* GETTERS */
	public String getFirstName() {
		return firstName.get();
	}
	
	public String getLastName() {
		return lastName.get();
	}
	
	public String getStreet() {
		return street.get();
	}
	
	public int getPostalCode() {
		return postalCode.get();
	}
	
	public String getCity() {
		return city.get();
	}
	
	@XmlJavaTypeAdapter(LocalDateAdapter.class)
	public LocalDate getBirthday() {
		return birthday.get();
	}
	
}
