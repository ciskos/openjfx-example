package io.ciskos.openjfxexample;

import java.io.File;
import java.io.IOException;
import java.util.prefs.Preferences;

import io.ciskos.openjfxexample.model.Person;
import io.ciskos.openjfxexample.model.PersonListWrapper;
import io.ciskos.openjfxexample.view.BirthdayStatisticsController;
import io.ciskos.openjfxexample.view.PersonEditDialogController;
import io.ciskos.openjfxexample.view.PersonOverviewController;
import io.ciskos.openjfxexample.view.RootLayoutController;
import jakarta.xml.bind.JAXBContext;
import jakarta.xml.bind.JAXBException;
import jakarta.xml.bind.Marshaller;
import jakarta.xml.bind.Unmarshaller;
import javafx.application.Application;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.image.Image;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.BorderPane;
import javafx.stage.Modality;
import javafx.stage.Stage;
import lombok.Getter;

/**
 * JavaFX App
 */
public class MainApp extends Application {

	private static final String PERSON_EDIT_DIALOG = "view/PersonEditDialog.fxml";
	private static final String PERSON_OVERVIEW = "view/PersonOverview.fxml";
	private static final String ROOT_LAYOUT = "view/RootLayout.fxml";
	@Getter
	private Stage primaryStage;
	private BorderPane rootLayout;
	@Getter
	private ObservableList<Person> personData = FXCollections.observableArrayList();

	public MainApp() {
		personData.add(new Person("Name1", "Surname1"));
		personData.add(new Person("Name2", "Surname2"));
		personData.add(new Person("Name3", "Surname3"));
		personData.add(new Person("Name4", "Surname4"));
		personData.add(new Person("Name5", "Surname5"));
		personData.add(new Person("Name6", "Surname6"));
		personData.add(new Person("Name7", "Surname7"));
		personData.add(new Person("Name8", "Surname8"));
		personData.add(new Person("Name9", "Surname9"));
	}
	
    public static void main(String[] args) throws JAXBException {
    	launch();
    }

    @Override
    public void start(Stage stage) throws IOException {
    	this.primaryStage = stage;
    	this.primaryStage.setTitle("AddressApp");
    	this.primaryStage.getIcons().add(new Image("file:resources/images/299047_address_book_icon.png"));
    	
    	initRootLayout();
    	showPersonOverview();
    }
    
    /**
     * Инициализирует корневой макет и пытается загрузить последний открытый
     * файл с адресатами.
     * */
    public void initRootLayout() throws IOException {
    	FXMLLoader loader = new FXMLLoader();
    	
    	// Загрузка корневого макета из fxml файла
    	loader.setLocation(MainApp.class.getResource(ROOT_LAYOUT));
    	rootLayout = (BorderPane)loader.load();
    	
    	// Отобразить сцену, содержащую корневой макет.
    	Scene scene = new Scene(rootLayout);
    	primaryStage.setScene(scene);
    	
    	// Даём котроллеру доступ к главному приложению.
    	RootLayoutController controller = loader.getController();
    	controller.setMainApp(this);
    	
    	primaryStage.show();
    	
    	// Пытается загрузить последний открытый файл с адрестами.
    	File file = getPersonFilePath();
    	
    	if (file != null) {
			loadPersonDataFromFile(file);
		}
    }

    // Показывает в корневом макете сведения об адресатах
	public void showPersonOverview() throws IOException {
		// Загружаем сведения об адресатах
		FXMLLoader loader = new FXMLLoader();
		
		loader.setLocation(MainApp.class.getResource(PERSON_OVERVIEW));
		AnchorPane personOverview = (AnchorPane)loader.load();
		
		// Помещаем сведения об адресатах в центр корневого макета.
		rootLayout.setCenter(personOverview);
		
		PersonOverviewController controller = loader.getController();
		controller.setMainApp(this);
	}
	
	/**
	 * Открывает диалоговое окно для изменения деталей указанного адресата.
	 * Если пользователь кликнул OK, то изменения сохраняются в представленном
	 * объекте адресата и возвращается значение true.
	 * 
	 * @param person - объект адресата, который надо изменить
	 * @return true, если пользователь кликнул OK, в противном случае false.
	 * */
	public boolean showPersonEditDialog(Person person) {
		try {
			// Загружаем fxml-файл и создаём новую сцену
			// для всплывающего диалогового окна.
			FXMLLoader loader = new FXMLLoader();

			loader.setLocation(MainApp.class.getResource(PERSON_EDIT_DIALOG));
			AnchorPane page = (AnchorPane)loader.load();

			// Создаём диалоговое окно Stage.
			Stage dialogStage = new Stage();
			dialogStage.setTitle("Edit Person");
			dialogStage.initModality(Modality.WINDOW_MODAL);
			dialogStage.initOwner(primaryStage);
			
			Scene scene = new Scene(page);
			dialogStage.setScene(scene);
			
			// Передаём адресата в контроллер.
			PersonEditDialogController controller = loader.getController();
			controller.setDialogStage(dialogStage);
			controller.setPerson(person);
			
			// Отображаем диалоговое окно и ждём, пока пользователь его не закроет
			dialogStage.showAndWait();
			
			return controller.isOkClicked();
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}

	/**
	 * Возвращает preference файла адресата, т.е., последний открытый файл.
	 * Этот preference считывается из реестра, специфичного для конкретной
	 * операционной системы. Если preference не был найден, то возвращается null.
	 * 
	 * @return
	 * */
	public File getPersonFilePath() {
		Preferences prefs = Preferences.userNodeForPackage(MainApp.class);
		String filePath = prefs.get("filePath", null);

		if (filePath != null) {
			return new File(filePath);
		} else {
			return null;
		}
	}

	/**
	 * Задаёт путь текущему загруженному файлу. Этот путь сохраняется
	 * в реестре, специфичном для конкретной операционной системы.
	 * 
	 * @param file - файл или null, чтобы удалить путь
	 * */
	public void setPersonalFilePath(File file) {
		Preferences prefs = Preferences.userNodeForPackage(MainApp.class);
		
		if (file != null) {
			prefs.put("filePath", file.getPath());
			
			// Обновление заглавия сцены.
			primaryStage.setTitle("AddressApp - " + file.getName());
		} else {
			prefs.remove("filePath");
			
			// Обновление заглавия сцены.
			primaryStage.setTitle("AddressApp");
		}
	}
	
	/**
	 * Загружает информацию об адресатах из указанного файла.
	 * Текущая информация об адресатах будет заменена.
	 * 
	 * @param file
	 * */
	public void loadPersonDataFromFile(File file) {
		try {
			JAXBContext context = JAXBContext.newInstance(PersonListWrapper.class);
			Unmarshaller um = context.createUnmarshaller();
			
			// Чтение XML из файла и демаршализация.
			PersonListWrapper wrapper = (PersonListWrapper) um.unmarshal(file);
			
			personData.clear();
			personData.addAll(wrapper.getPersons());
			
			// Сохраняем путь к файлу в реестре.
			setPersonalFilePath(file);
		} catch (Exception e) {
			Alert alert = new Alert(AlertType.ERROR);
			
			alert.setTitle("Error");
			alert.setHeaderText("Could not load data");
			alert.setContentText("Could not load data from file:\n" + file.getPath());
			
			alert.showAndWait();
		}
	}
	
	/**
	 * Сохраняет текущую информацию об адресатах в указанном файле.
	 * 
	 * @param file
	 * */
	public void savePersonDataToFile(File file) {
		try {
			JAXBContext context = JAXBContext.newInstance(PersonListWrapper.class);
			Marshaller m = context.createMarshaller();
			
			m.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
			
			// Обёртываем наши данные об адресатах.
			PersonListWrapper wrapper = new PersonListWrapper();
			
			wrapper.setPersons(personData);
			
			// Маршаллируем и сохраняем XML в файл.
			m.marshal(wrapper, file);
			
			// Сохраняем путь к файлу в реестре.
			setPersonalFilePath(file);
		} catch (Exception e) {
			e.printStackTrace();
			
			Alert alert = new Alert(AlertType.ERROR);
			
			alert.setTitle("Error");
			alert.setHeaderText("Could not save data");
			alert.setContentText("Could not save data to file:\n" + file.getPath());
			
			alert.showAndWait();
		}
	}
	
	/**
	 * Открывает диалоговое окно для вывода статистики дней рождений.
	 * @throws IOException 
	 * */
	public void showBirthdayStatistics() throws IOException {
		// Загружает fxml-файл и создаёт новую сцену для всплывающего окна.
		FXMLLoader loader = new FXMLLoader();
		
		loader.setLocation(MainApp.class.getResource("view/BirthdayStatistics.fxml"));
		
		AnchorPane page = (AnchorPane)loader.load();
		Stage dialogStage = new Stage();
		
		dialogStage.setTitle("Birthday Statistics");
		dialogStage.initModality(Modality.WINDOW_MODAL);
		dialogStage.initOwner(primaryStage);
		
		Scene scene = new Scene(page);
		
		dialogStage.setScene(scene);
		
		// Передаёт адресатов в контроллер.
		BirthdayStatisticsController controller = loader.getController();
		
		controller.setPersonData(personData);
		
		dialogStage.show();
	}
	
}